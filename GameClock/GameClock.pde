/*
 * Wiring/arduino prototype for an AVR-based game clock.
 *
 * Copyright 2011 Yann Dirson <ydirson@free.fr>
 *
 * Licensed as GPLv2.
 */

/*
 * Done:
 *
 * - japanese byo-yomi clock for 2 players
 * - display on serial LCD
 * - pausing/restarting the game
 * - choice of clock durations within sets of predefined values
 *
 * User interface:
 * - in CONFIG mode (default at startup):
 *   - D0 = switches to PLAY mode
 *   - D1 = cycle through settings
 *   - D2 = cycle through predefined setting values
 * - in PLAY mode:
 *   - D0 = PAUSE, again switches to CONFIG mode
 *   - D1/D2 = players
 *
 * Known bugs:
 * - first player gets stolen one second
 * - after pausing, any player may signal restart of opponent's clock
 * - maintime=0 has a problem, we offer 1s instead as a workaround
 *
 */

/*
 * Hardware prototype setup:
 * - Atmega32u4 breakout board from Adafruit, USB-powered
 *   => onboard LED as WLED (E6)
 * - buttons between ground and D0/D1/D2 (aka EI0/EI1/EI2), each
 *   debounced with a 100nF capacitor
 * - SerialLCD screen on B2/B3
 * MCU resources:
 * - Timer0 used by NewSoftSerial used by SerialLCD (where, how !?)
 * - Timer1 used for time measurement
 */


// The program is built around the idea of a state machine, with 2
// main states:
enum {
  PLAY,
  CONFIG,
} mode = CONFIG;

// Within the mode=PLAY state, the clock may be running for one player
// or the other, or be stopped:
int running = -1;

// Within the mode=PLAY state, the selected clock mechanism is itself
// handled with several states:
typedef enum {
  MAINTIME,
  BYOYOMI,
} phase_t;

// Within the mode=CONFIG state, what gets configured is itself
// handled with several states:
enum cfg_state_t {
  CFG_DURATION,
  CFG_BY_PERIODS,
  CFG_BY_DURATION,

  CFG_STATES_MAX   // must be last
} cfg_state = CFG_DURATION;

/*
 * Data structures
 */

typedef struct player {
  uint16_t remaining_secs;
  uint16_t remaining_cycles;
  phase_t  phase;
  uint8_t  remaining_byoyomi;
} player_t;

player_t players[2];


// a handful of predefined game durations
uint16_t durations[] = { 0, 15, 30, 45, 60, 90 }; // min
uint8_t duration_choice = 2;
uint8_t byoyomi_durations[] = { 5, 10, 15, 30, 60 }; // sec
uint8_t byoyomi_duration_choice = 4;
uint8_t byoyomi_periods[] = { 0, 5, 10 }; // nb
uint8_t byoyomi_period_choice = 1;

/*
 * LCD screen
 */

#include <SerialLCD.h>
#include <NewSoftSerial.h>
SerialLCD slcd(pinB2,pinB3);
static char linebuffer[16];

void slcd_print_time(struct player *p)
{
  sprintf(linebuffer, "%2um %2us +%2u*%2us",
	  p->remaining_secs / 60, p->remaining_secs % 60,
	  p->remaining_byoyomi, byoyomi_durations[byoyomi_duration_choice]);
  slcd.print(linebuffer);
}

// LCD shall be cleared before first update
bool slcd_clear = true;

void slcd_update()
{
  if (slcd_clear) {
    slcd.clear();
    slcd_clear = 0;
  }
  switch (mode) {
  case PLAY:
    slcd.setCursor(0,0);
    slcd_print_time(&players[0]);
    slcd.setCursor(0,1);
    slcd_print_time(&players[1]);
    break;
  case CONFIG:
    slcd.setCursor(0,0);
    slcd.print("Duration:");
    slcd.setCursor(0,1);
    sprintf(linebuffer, "%c%2um +%c%2u *%c%2us",
	    cfg_state == CFG_DURATION ? '>' : ' ',
	    durations[duration_choice],
	    cfg_state == CFG_BY_PERIODS ? '>' : ' ',
	    byoyomi_periods[byoyomi_period_choice],
	    cfg_state == CFG_BY_DURATION ? '>' : ' ',
	    byoyomi_durations[byoyomi_duration_choice]);
    slcd.print(linebuffer);
    break;
  }
}

/*
 * Interrupt handlers and their helpers
 */

static inline void toggle()
{
  static int led = LOW;
  led = (led==LOW) ? HIGH : LOW;
  digitalWrite(WLED, led);
}

static void tick()
{
  toggle();
  if (running >= 0) {
    players[running].remaining_secs --;
  }
  if (players[running].remaining_secs == 0) {
    if (players[running].phase == MAINTIME)
      players[running].phase = BYOYOMI;
    else
      players[running].remaining_byoyomi --;
    if (players[running].remaining_byoyomi > 0) {
      players[running].remaining_secs = \
	byoyomi_durations[byoyomi_duration_choice];
    }
    if (players[running].remaining_secs == 0)
      stop_button();
  }
}

void take_turn(int player) {
  if (((players[0].remaining_secs == 0) &&
       (players[0].remaining_byoyomi == 0)) ||
      ((players[1].remaining_secs == 0) &&
       (players[1].remaining_byoyomi == 0)))
    // game has already finished
    return;
  if (running != player) {
    // store player state
    players[running].remaining_cycles = Timer1.getCounter();
    if (players[running].phase == BYOYOMI)
      players[running].remaining_secs = \
	byoyomi_durations[byoyomi_duration_choice];
    // change player, restore state
    running = player;
    toggle();
    Timer1.setCounter(players[running].remaining_cycles);
    players[running].remaining_cycles = 0;
    Timer1.enableInterrupt(INTERRUPT_COMPARE_MATCH_A);
  }
}

/*
 * Button handling is different according to the mode:
 * - when playing, STOP pauses the game, but switches to CONFIG if already
 *   in PAUSE
 * - in CONFIG, button0 switches time, and STOP returns to PLAY mode
 */

static void button0()
{
  switch (mode) {
  case PLAY:
    take_turn(1);
    break;
  case CONFIG:
    cfg_state = (cfg_state_t)((cfg_state + 1) % CFG_STATES_MAX);
    break;
  }
}
static void button1()
{
  switch (mode) {
  case PLAY:
    take_turn(0);
    break;
  case CONFIG:
    switch (cfg_state) {
    case CFG_DURATION:
      duration_choice = (duration_choice + 1) %
	(sizeof(durations) / (sizeof(durations[0])));
      break;
    case CFG_BY_PERIODS:
      byoyomi_period_choice = (byoyomi_period_choice + 1) %
	(sizeof(byoyomi_periods) / (sizeof(byoyomi_periods[0])));
      break;
    case CFG_BY_DURATION:
      byoyomi_duration_choice = (byoyomi_duration_choice + 1) %
	(sizeof(byoyomi_durations) / (sizeof(byoyomi_durations[0])));
      break;
    }
    break;
  }
}

static void stop_button()
{
  switch (mode) {
  case PLAY:
    if (running >= 0) {
      Timer1.disableInterrupt(INTERRUPT_COMPARE_MATCH_A);
      running = -1;
    } else {
      mode = CONFIG;
    }
    break;
  case CONFIG:
    mode = PLAY;
    // set new game settings
    // FIXME: should get confirmation
    uint16_t duration = durations[duration_choice] * 60;
    // FIXME: special case to workaround startup bug
    if (duration == 0) duration = 1;

    players[0].remaining_secs = players[1].remaining_secs = duration;
    players[0].remaining_byoyomi = players[1].remaining_byoyomi = \
      byoyomi_periods[byoyomi_period_choice];
    players[0].remaining_cycles = players[1].remaining_cycles = 0;
    break;
  }
  slcd_clear = true;
}

/*
 * Initialization code
 */

void setup() {
  pinMode(WLED, OUTPUT);

  // setup buttons via external interrupts, pullups activated
  pinMode(EI0, INPUT); pullup(EI0);
  pinMode(EI1, INPUT); pullup(EI1);
  pinMode(EI2, INPUT); pullup(EI2);
  attachInterrupt(EXTERNAL_INTERRUPT_0, stop_button, FALLING);
  attachInterrupt(EXTERNAL_INTERRUPT_1, button0, FALLING);
  attachInterrupt(EXTERNAL_INTERRUPT_2, button1, FALLING);

  // setup timer/counter
  Timer1.setMode(0b0100);  // CTC mode with OCR1A
  Timer1.setClockSource(CLOCK_PRESCALE_1024);
  Timer1.setOCR(CHANNEL_A, 16000000/1024);
  Timer1.attachInterrupt(INTERRUPT_COMPARE_MATCH_A, tick);
  // be sure the clock is stopped to start with
  Timer1.disableInterrupt(INTERRUPT_COMPARE_MATCH_A);

  // disable unused clocks
  //Timer0.setClockSource(CLOCK_STOP);
  Timer3.setClockSource(CLOCK_STOP);

  // start comm with LCD screen
  slcd.begin();
  slcd.print("GameClock ready!");
  delay(1000);
}

void loop() {
  slcd_update();
  sleep_mode();
}
