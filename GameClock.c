/*
 * AVR-libc prototype for an AVR-based game clock.
 *
 * Copyright 2011, 2015 Yann Dirson <ydirson@free.fr>
 *
 * Licensed as GPLv2.
 */

/*
 * Hardware prototype setup 1:
 * - Atmega32u4 breakout board from Adafruit, USB-powered
 *   => onboard LED (E6)
 * - 3 buttons between ground and PB0/PB1/PB2 (aka PCINT0/1/2), each
 *   debounced with a 100nF capacitor
 * - I2C ssd1306-driven heltec.cn OLED via u8glib on TWI (PD0/PD1)
 * Hardware prototype setup 2:
 * - Arduino Uno, USB-powered
 *   => onboard LED (B5)
 * - 3 buttons between ground and pins 8/10/10 (aka PB0/PB1/PB2 aka PCINT0/1/2), each
 *   debounced with a 100nF capacitor
 * - I2C ssd1306-driven heltec.cn OLED via u8glib on TWI (PC4/PC5)
 * MCU resources:
 * - Timer1 used for time measurement
 */

/*
 * To be ported from Wiring proto:
 * - display on serial LCD (obsolete, for fun and for an alternate
 *   display platform to debug with different interactions)
 *
 * Known bugs:
 * - bad interaction with OLED:
 *   - on m32u4 with EINT's (old code), but not on m328
 *     - LED does not blink any more
 *     - autostops after 2sec after button presses
 *   - default u8g build pulls loads of unused code
 * - with PCINT's, and last_state in DATA section, several sequences confuse
 *   the button state just as if the memory was not reliable
 *   - eg. btn0, btn2, btn1 => btn2 seen as released a 2nd time
 *   - eg. right on powerup, btn1 => btn2 seen as released:
[2015-04-15 23:30:58.088] PCINT: fd 101 (last=111)
[2015-04-15 23:30:58.117] TIMSK1=00
[2015-04-15 23:30:58.129] => last=111
[2015-04-15 23:30:58.146]  btn1 pressed
[2015-04-15 23:30:58.162] => last=101
[2015-04-15 23:30:58.175] => last=101
[2015-04-15 23:30:58.350] PCINT: ff 111 (last=101)
[2015-04-15 23:30:58.379] TIMSK1=00
[2015-04-15 23:30:58.391] => last=101
[2015-04-15 23:30:58.408]  btn1 released
[2015-04-15 23:30:58.424] => last=001
[2015-04-15 23:30:58.441]  btn2 released
[2015-04-15 23:30:58.457] => last=101

  => incriminates "last_state.btn1 = ISSET(pinreg, PIN_BTN1);" ?!

 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <util/delay.h>
#include <stdio.h>

#include "misc.h"

/* bitfield:
 * 1=buttons (press/release events in irq handler)
 * 2="verbose" (last_state heavy change tracking in irq handler hunting for corruption)
 */
#define DEBUG_USART 0

#define CLR(REG,BIT) do { (REG) &= ~_BV(BIT); } while(0)
#define SET(REG,BIT) do { (REG) |= _BV(BIT); } while(0)
#define ISSET(REG,BIT) ((uint8_t)((REG & _BV(BIT)) ? 1:0))
#define SETFIELD(REG,MASK,BITS) do { (REG) = ((REG) & ~(MASK)) | (BITS); } while(0)

#include BOARDHEADER

static void stop();

#if DEBUG_USART
/*
 * debug via USART
 */

void usart_init()
{
  /* Set baud rate to 9600 @16MHz */
  UBRR_CONSOLE = 103;
  /* Enable receiver and transmitter */
  UCSRB_CONSOLE = _BV(RXEN_CONSOLE)|_BV(TXEN_CONSOLE);
  /* Set frame format: 8data, 2stop bit */
  UCSRC_CONSOLE = _BV(USBS_CONSOLE)|(3<<UCSZ0_CONSOLE);
}
void usart_putchar(unsigned char data)
{
  /* Wait for empty transmit buffer */
  while ( !( UCSRA_CONSOLE & _BV(UDRE_CONSOLE)) );
  /* Put data into buffer, sends the data */
  UDR_CONSOLE = data;
}

static FILE mystdout = FDEV_SETUP_STREAM(usart_putchar, NULL,
                                         _FDEV_SETUP_WRITE);

#endif
#if DEBUG_USART & 1
# define dbg_btn(args...) printf(args)
#else
# define dbg_btn(args...) do {} while(0)
#endif
#if DEBUG_USART & 2
# define dbg_verb(args...) printf(args)
#else
# define dbg_verb(args...) do {} while(0)
#endif

/*
 * Data structures
 */

// The program is built around the idea of a state machine, with 2
// main states:
enum {
  PLAY,
  CONFIG,
} mode = CONFIG;

// Within the mode=PLAY state, the selected clock mechanism is itself
// handled with several states:
typedef enum {
  MAINTIME,
  BYOYOMI,
} phase_t;

// Within the mode=CONFIG state, what gets configured is itself
// handled with several states:
enum cfg_state_t {
  CFG_DURATION,
  CFG_BY_PERIODS,
  CFG_BY_DURATION,

  CFG_STATES_MAX   // must be last
} cfg_state = CFG_DURATION;

typedef struct player {
  uint16_t remaining_secs;
  uint8_t remaining_cycles_lo, remaining_cycles_hi;
  uint8_t outoftime;
  // byoyomi handling
  phase_t  phase;
  uint8_t  remaining_byoyomi;
} player_t;

player_t players[2] = { { .remaining_secs = 60 },
                        { .remaining_secs = 60 } }; // 60s each for a test

/**/

static inline void screen_update_play();
static inline void screen_update_config();

/*
 * OLED display
 */

#include "u8g.h"

u8g_t u8g;
static char linebuffer[16];

void screen_update()
{
  switch (mode) {
  case PLAY:
    screen_update_play();
    break;
  case CONFIG:
    screen_update_config();
    break;
  }
}


/*
 * generic infrastructure
 */

static inline void toggle()
{
  PORT_LED ^= _BV(PIN_LED);
}

static void timer1_enable()
{
  // timer overflow interrupt enable (Output Compare A Match)
  TIMSK1 = _BV(OCIE1A);
}
static void timer1_disable()
{
  // timer overflow interrupt enable (Output Compare A Match)
  TIMSK1 = 0;
}

static union {
  struct {
    uint8_t btn0:1;
    uint8_t btn1:1;
    uint8_t btn2:1;
  };
  uint8_t all:3;
} last_state
#if 0
asm("r3")
#else
  //= { .all = 0b111 };
#endif
;


/*
 * CONFIG mode
 */

// a handful of predefined game durations
uint16_t durations[] = { 0, 1, 15, 30, 45, 60, 90 }; // min
uint8_t duration_choice = 1;
uint8_t byoyomi_durations[] = { 5, 10, 15, 30, 60 }; // sec
uint8_t byoyomi_duration_choice = 0;
uint8_t byoyomi_periods[] = { 0, 1, 5, 10 }; // nb
uint8_t byoyomi_period_choice = 1;

static inline u8g_uint_t DrawStr_maybe_reversed(u8g_t *u8g, u8g_uint_t x, u8g_uint_t y,
                                          const char *s, uint8_t reversed)
{
  u8g_uint_t w;
  if (reversed) {
    w = u8g_GetStrWidth(u8g, s);
    //u8g_SetColorIndex(u8g, 1);
    u8g_DrawBox(u8g, x, y - u8g_GetFontAscent(u8g) - 1,
                // FIXME: why do we need "+2" instead of "+1" ?
                w, u8g_GetFontAscent(u8g) /* - u8g_GetFontDescent(u8g) */ + 2);
    u8g_SetColorIndex(u8g, 0);
    w = u8g_DrawStr(u8g, x, y, s);
    u8g_SetColorIndex(u8g, 1);
  } else {
    w = u8g_DrawStr(u8g, x, y, s);
  }
  return w;
}

static inline void screen_update_config()
{
  u8g_SetFont(&u8g, u8g_font_timB18);
  u8g_DrawStr(&u8g, 0, 20, "Duration:");

  u8g_SetFont(&u8g, u8g_font_timB14);
  u8g_uint_t x = 0; // start at full left

  sprintf(linebuffer, "%um", durations[duration_choice]);
  x += DrawStr_maybe_reversed(&u8g, x, 50, linebuffer, cfg_state == CFG_DURATION);

  sprintf(linebuffer, "+%u", byoyomi_periods[byoyomi_period_choice]);
  x += DrawStr_maybe_reversed(&u8g, x, 50, linebuffer, cfg_state == CFG_BY_PERIODS);

  sprintf(linebuffer, "*%us", byoyomi_durations[byoyomi_duration_choice]);
  x += DrawStr_maybe_reversed(&u8g, x, 50, linebuffer, cfg_state == CFG_BY_DURATION);
}

static inline void config_use()
{
  mode = PLAY;
  // set new game settings
  // FIXME: should get confirmation
  uint16_t duration = durations[duration_choice] * 60;
  // FIXME: special case to workaround startup bug with maintime=0
  if (duration == 0) duration = 1;

  players[0].remaining_secs = players[1].remaining_secs = duration;
  players[0].remaining_byoyomi = players[1].remaining_byoyomi =
    byoyomi_periods[byoyomi_period_choice];
  players[0].remaining_cycles_hi = players[1].remaining_cycles_hi = 0;
  players[0].remaining_cycles_lo = players[1].remaining_cycles_lo = 0;
  players[0].outoftime = players[1].outoftime = 0;
}

static inline void config_next_setting()
{
  cfg_state = (enum cfg_state_t)((cfg_state + 1) % CFG_STATES_MAX);
}

static inline void config_next_preset()
{
  switch (cfg_state) {
  case CFG_DURATION:
    duration_choice = (duration_choice + 1) %
      (sizeof(durations) / (sizeof(durations[0])));
    break;
  case CFG_BY_PERIODS:
    byoyomi_period_choice = (byoyomi_period_choice + 1) %
      (sizeof(byoyomi_periods) / (sizeof(byoyomi_periods[0])));
    break;
  case CFG_BY_DURATION:
    byoyomi_duration_choice = (byoyomi_duration_choice + 1) %
      (sizeof(byoyomi_durations) / (sizeof(byoyomi_durations[0])));
    break;
  }
}


/*
 * PLAY mode
 */

// Within the mode=PLAY state, the clock may be running for one player
// or the other, or be stopped (-1):
int8_t running = -1;

static inline void DrawStrCentered(u8g_uint_t x, u8g_uint_t y, const char* s)
{
  u8g_uint_t width = u8g_GetStrWidth(&u8g, s);
  u8g_DrawStr(&u8g, x - width/2, y, s);
}

static inline void print_time(struct player *p, u8g_uint_t x)
{
  uint16_t mm;
  uint8_t h, m, s;
  mm = p->remaining_secs / 60; s = p->remaining_secs % 60;
  h = mm / 60; m = mm % 60;

  if (mm) { // 1h02 | 40'
    if (h) {
      sprintf(linebuffer, "%uh%02u", h, m);
      u8g_SetFont(&u8g, u8g_font_timB18);
      DrawStrCentered(x + 31, 19, linebuffer);
    } else {
      sprintf(linebuffer, "%u'", m);
      u8g_SetFont(&u8g, u8g_font_timB24);
      DrawStrCentered(x + 31, 25, linebuffer);
    }
    // seconds and byoyomi
    // FIXME: still too much on one line
    sprintf(linebuffer, "%us+%u*%us", s,
            p->remaining_byoyomi, byoyomi_durations[byoyomi_duration_choice]);
    u8g_SetFont(&u8g, u8g_font_timB10);
    DrawStrCentered(x + 31, 62, linebuffer);
  } else { // 30s
    sprintf(linebuffer, "%us", s);
    u8g_SetFont(&u8g, u8g_font_timB24);
    DrawStrCentered(x + 31, 25, linebuffer);
    // byoyomi
    sprintf(linebuffer, "+%u*%us",
            p->remaining_byoyomi, byoyomi_durations[byoyomi_duration_choice]);
    u8g_SetFont(&u8g, u8g_font_timB14);
    DrawStrCentered(x + 31, 62, linebuffer);
  }

  if (p->outoftime) {
    u8g_DrawLine(&u8g, x, 0, x+63, 63);
    u8g_DrawLine(&u8g, x, 63, x+63, 0);
  }
}

static inline void screen_update_play()
{
  u8g_SetFont(&u8g, u8g_font_timB14);
  print_time(&players[0], 0);
  print_time(&players[1], 65);
  u8g_DrawVLine(&u8g, 63, 0, 64);
  // who's turn
  if (running >= 0)
    u8g_DrawTriangle(&u8g, 63, 40, 63, 50,
                     running ? 73 : 53, 45);
}

void take_turn(int player) {
  if (((players[0].remaining_secs == 0) &&
       (players[0].remaining_byoyomi == 0)) ||
      ((players[1].remaining_secs == 0) &&
       (players[1].remaining_byoyomi == 0)))
    // game has already finished
    return;
  if (running != player) {
    // store player state
    players[running].remaining_cycles_lo = TCNT1L;
    players[running].remaining_cycles_hi = TCNT1H;
    if (players[running].phase == BYOYOMI)
      players[running].remaining_secs = \
	byoyomi_durations[byoyomi_duration_choice];
    // change player, restore state
    running = player;
    toggle();
    TCNT1L = players[running].remaining_cycles_lo;
    TCNT1H = players[running].remaining_cycles_hi;
    players[running].remaining_cycles_lo = 0;
    players[running].remaining_cycles_hi = 0;
    timer1_enable();
  }
}

static void stop()
{
  if (running >= 0) {
    timer1_disable();
    running = -1;
  }
}

// clock tick
ISR(TIMER1_COMPA_vect)
{
  toggle();
  if (running >= 0) {
    players[running].remaining_secs --;
  }
  if (players[running].remaining_secs == 0) {
    if (players[running].phase == MAINTIME)
      players[running].phase = BYOYOMI;
    else
      players[running].remaining_byoyomi --;
    if (players[running].remaining_byoyomi > 0) {
      players[running].remaining_secs = \
	byoyomi_durations[byoyomi_duration_choice];
    }
    if (players[running].remaining_secs == 0) {
      players[running].outoftime = 1;
      stop();
    }
  }
}

/*
 * Button handling is different according to the mode:
 * - when playing, STOP pauses the game, but switches to CONFIG if already
 *   in PAUSE
 * - in CONFIG, button0 switches time, and STOP returns to PLAY mode
 */
ISR(PCINT0_vect)
{
  uint8_t pinreg = PINREG_BTN0;
  ct_assert(&PINREG_BTN1 == &PINREG_BTN0);
  ct_assert(&PINREG_BTN2 == &PINREG_BTN0);

  dbg_verb("PCINT: %02x %d%d%d (last=%d%d%d)\r\n", pinreg,
           ISSET(pinreg, PIN_BTN2), ISSET(pinreg, PIN_BTN1), ISSET(pinreg, PIN_BTN0),
           last_state.btn2, last_state.btn1, last_state.btn0);
  dbg_verb("TIMSK1=%02x\r\n", TIMSK1);

  if (ISSET(pinreg, PIN_BTN0) != last_state.btn0) {
    dbg_btn(" btn0 ");
    last_state.btn0 = ISSET(pinreg, PIN_BTN0);
    if (last_state.btn0) {
      dbg_btn("released\r\n");
      switch (mode) {
      case PLAY:
        take_turn(0);
        break;
      case CONFIG:
        config_next_setting();
        break;
      }
    } else {
      dbg_btn("pressed\r\n");
    }
  }
  dbg_verb("=> last=%d%d%d\r\n", last_state.btn2, last_state.btn1, last_state.btn0);
  if (ISSET(pinreg, PIN_BTN1) != last_state.btn1) {
    dbg_btn(" btn1 ");
    last_state.btn1 = ISSET(pinreg, PIN_BTN1);
    if (last_state.btn1) {
      dbg_btn("released\r\n");
      switch (mode) {
      case PLAY:
        take_turn(1);
        break;
      case CONFIG:
        config_next_preset();
        break;
      }
    } else {
      dbg_btn("pressed\r\n");
    }
  }
  dbg_verb("=> last=%d%d%d\r\n", last_state.btn2, last_state.btn1, last_state.btn0);
  if (ISSET(pinreg, PIN_BTN2) != last_state.btn2) {
    dbg_btn(" btn2 ");
    last_state.btn2 = ISSET(pinreg, PIN_BTN2);
    if (last_state.btn2) {
      dbg_btn("released\r\n");
      switch (mode) {
      case PLAY:
        if (running >= 0) {
          stop();
        } else {
          mode = CONFIG;
        }
        break;
      case CONFIG:
        config_use();
        break;
      }
    } else {
      dbg_btn("pressed\r\n");
    }
  }
  dbg_verb("=> last=%d%d%d\r\n", last_state.btn2, last_state.btn1, last_state.btn0);
}

int main()
{
  // initial value for globals (required to use as "register" or as
  // "BSS", since initialized "DATA" apparently causes problems)
  last_state.all = 0b111;

  // wait a bit to ensure we can talk to the display
  _delay_ms(100);

#if DEBUG_USART
  usart_init();
  stdout = &mystdout;
#endif

  /* Onboard LED */
  DDR_LED = _BV (PIN_LED);

  /*
   * BUTTONS
   */

  // pins as input
  CLR(DDR_BTN0, PIN_BTN0);
  CLR(DDR_BTN1, PIN_BTN1);
  CLR(DDR_BTN2, PIN_BTN2);
  // enable pullups on whole port
  ct_assert(&PORT_BTN1 == &PORT_BTN0);
  ct_assert(&PORT_BTN2 == &PORT_BTN0);
  PORT_BTN0 = 0xFF;
  // pin-change interrupts on PB0-PB2
  SET(PCICR, PCIE0);
  SET(PCMSK0, PCINT0);
  SET(PCMSK0, PCINT1);
  SET(PCMSK0, PCINT2);

  /*
   * Clock timer
   */

  timer1_disable();
  // set Timer1 in CTC mode with OCR1A, clk/1024
  TCCR1A = 0; TCCR1B = _BV(WGM12) | _BV(CS12) | _BV(CS10);
  // output-compare register A
  uint16_t threshold = 15625; // 16 MHz / 1024
  OCR1AH = threshold >> 8; OCR1AL = threshold & 0xFF;
  // timer counter set to 0
  TCNT1L = 0; TCNT1H = 0;

  /*
   * Display
   */
  u8g_InitI2C(&u8g, &u8g_dev_ssd1306_128x64_i2c, U8G_I2C_OPT_NO_ACK);

  /* go! */

  sei();
  while(1) {
    u8g_FirstPage(&u8g);
    do {
      screen_update();
    } while ( u8g_NextPage(&u8g) );
    sleep_mode();
  }
}
