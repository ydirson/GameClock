TGT = 32u4
#TGT = uno

# uncomment to use reduced u8g fonts, see bdfsubset.py for subset definition
# comment out to use the full fonts
REDUCED=_reduced

ifeq ($(TGT), 32u4)
MCU=atmega32u4
PROGRAMMER=avr109
TTYS=/dev/serial/by-id/usb-239a_AVR_CDC_Bootloader-if00
F_CPU = 16000000L
BOARDHEADER = board_adafruit32u4.h

else ifeq ($(TGT), uno)
MCU=atmega328p
PROGRAMMER=arduino
TTYS=/dev/serial/by-id/usb-Arduino__www.arduino.cc__Arduino_Uno_6493534323335151A211-if00
F_CPU = 16000000L
BOARDHEADER = board_arduino_uno.h

else
$(error unknown TGT '$(TGT)')
endif

PRG=GameClock
BIN = bin-$(TGT)/$(PRG).bin
INCLUDES = -DBOARDHEADER='"$(BOARDHEADER)"' -I lib/u8glib/csrc/
VPATH = lib/u8glib/csrc/:lib/u8glib/fntsrc/
_OBJS = \
	GameClock.o \
	u8g_font_timb10$(REDUCED).o \
	u8g_font_timb14$(REDUCED).o \
	u8g_font_timb18$(REDUCED).o \
	u8g_font_timb24$(REDUCED).o

_LIBOBJS = \
	u8g_delay.o \
	u8g_ll_api.o \
	u8g_font.o \
	u8g_clip.o \
	u8g_com_api.o \
	u8g_com_io.o \
	u8g_com_i2c.o \
	u8g_pb.o \
	u8g_state.o \
	u8g_com_atmega_hw_spi.o \
	u8g_com_atmega_sw_spi.o \
	u8g_com_arduino_ssd_i2c.o \
	u8g_dev_ssd1306_128x64.o \
	u8g_rect.o \
	u8g_line.o \
	u8g_polygon.o \
	u8g_pb8v1.o \
	u8g_pb16v1.o \
	u8g_page.o
OBJS = $(_OBJS:%=bin-$(TGT)/%)
LIBOBJS = $(_LIBOBJS:%=bin-$(TGT)/%)

CC	= avr-gcc
OBJCOPY = avr-objcopy
OBJDUMP = avr-objdump
CFLAGS	= -W -Wall -g -Os -mmcu=$(MCU) -DF_CPU=$(F_CPU)
CFLAGS += -funsigned-char -funsigned-bitfields -fpack-struct -fshort-enums
CFLAGS += -ffunction-sections -fdata-sections -Wl,--gc-sections
CFLAGS += -Wl,--relax
CFLAGS += -mcall-prologues
CPPFLAGS= $(INCLUDES)
LDFLAGS	= -Wl,-Map,$(BIN:.bin=.map)

default: $(BIN)

# override default rule to build in subdir
$(OBJS) $(LIBOBJS): bin-$(TGT)/%.o: %.c $(BOARDHEADER)
	mkdir -p $(dir $@)
	$(COMPILE.c) -Wa,-adhlns=$(@:.o=.S) -o $@ $<

$(BIN:.bin=.elf): $(OBJS) $(LIBOBJS)
	$(CC) $(CFLAGS) $(LDFLAGS) $^ -o $@

%.lst: %.elf
	$(OBJDUMP) -h -S $< > $@

%.bin.S: %.elf
	$(OBJDUMP) -S $< > $@

%.hex: %.elf %.bin.S
	$(OBJCOPY) -j .text -j .data -O ihex $< $@

%.srec: %.elf %.bin.S
	$(OBJCOPY) -j .text -j .data -O srec $< $@

%.bin: %.elf %.bin.S
	$(OBJCOPY) -j .text -j .data -O binary $< $@

clean:
#	rm -f *.o *.elf *.lst *.map
#	rm -f *.hex *.bin *.srec
	rm -rf bin-*/
	rm -f bdf2u8g *-reduced.bdf u8g_font_*_reduced.c

flash: $(BIN:.bin=.flash)

%.flash: %.bin
	avrdude -p $(MCU) -c $(PROGRAMMER) -P $(TTYS) -U flash:w:$(<)
.PRECIOUS: %.bin %.bin.S %.S
.PHONY: %.flash

### fonts

%-reduced.bdf: lib/u8glib/tools/font/bdf/%.bdf
	./bdfsubset.py $< $@
u8g_font_timb%_reduced.c: timB%-reduced.bdf bdf2u8g
	./bdf2u8g $< u8g_font_timB$* $@ > /dev/null

# host tool
bdf2u8g: lib/u8glib/tools/font/bdf2u8g/bdf2u8g.c
	gcc $< -o $@
