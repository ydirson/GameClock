#define PIN_LED  (PINB5)
#define PORT_LED (PORTB)
#define DDR_LED  (DDRB)

#define UBRR_CONSOLE (UBRR0)
#define UCSRA_CONSOLE (UCSR0A)
#define UCSRB_CONSOLE (UCSR0B)
#define UCSRC_CONSOLE (UCSR0C)
#define RXEN_CONSOLE (RXEN0)
#define TXEN_CONSOLE (TXEN0)
#define USBS_CONSOLE (USBS0)
#define UCSZ0_CONSOLE (UCSZ00)
#define UDRE_CONSOLE (UDRE0)
#define UDR_CONSOLE (UDR0)

// TWI is C4/C5

// pin 8
#define PIN_BTN0  (PINB0)
#define PINREG_BTN0 (PINB)
#define PORT_BTN0 (PORTB)
#define DDR_BTN0  (DDRB)

// pin 9
#define PIN_BTN1  (PINB1)
#define PINREG_BTN1 (PINB)
#define PORT_BTN1 (PORTB)
#define DDR_BTN1  (DDRB)

// pin 10
#define PIN_BTN2  (PINB2)
#define PINREG_BTN2 (PINB)
#define PORT_BTN2 (PORTB)
#define DDR_BTN2  (DDRB)
