#!/usr/bin/python

import sys
_, input, output = sys.argv

# characters to keep - must obviously be in sync with the code :)
keep = ('zero one two three four five six seven eight nine '
        'plus minus asterisk colon quotesingle'
        ).split(' ') + [ letter for letter in "hmsDuration"]

# do filter BDF file
with open(input, "r") as inf:
    with open(output, "w") as outf:
        skipping = False
        for line in inf:
            if line.startswith("STARTCHAR ") and line.strip()[10:] not in keep:
                skipping = True
            if skipping:
                if line.startswith("ENDCHAR"):
                    skipping = False
                continue
            print >>outf, line,
