Game Clock
==========

The project: designing an robust and ultraportable electronic device
to assist in playing real-life games with a time limitation.

Planned hardware features:

* monochrome OLED screen (readability, low energy requirements)
* touch buttons (no mechanical parts for robustness)
* AVR microcontroller (opensource-friendly, lots of available stuff)
* LiPo battery rechargeable from a USB port
* as hackable as reasonable
* Free/Open-source hardware

Planned software features:

* multiple clock mechanism (Standard Chess/Go/Shogi clocks, Fisher clock, etc)
* possible use of different time settings for each player (time handicap)
* pause/restart the clock
* save state to EEPROM
* USB console available for debug
* Free/Open-source software

Hardware
--------

There are currently 2 prototype setups, built respectively around the
Atmega32u4 breakout board from Adafruit (small, inexpensive, powerful
microcontroller), and around the Arduino Uno (more commonly found,
different microcontroller for comparisons).

Both prototypes include the same equipment around the main board:

* 3 microswitch buttons (each debounced with a 100nF capacitor)
* an I2C ssd1306-driven heltec.cn OLED (very low-cost, 0.96in)

The hardware setup is currently described in more details in the
source files.  More will be added in the repository as the design
evolves.

Firmware
--------

The software is written in C using avr-libc.  Takes a bit more time
than the Arduino runtime to get something to work, but allows to produce
much better software in the (not so) long run.

The first software prototype (for a hardware prototype using a
character-based LCD display) was based on the Wiring runtime (the
little-known precursor to Arduino, now mostly defunct), and its
features still have to be ported to the new software.

### Clock types ###

The current firmware provides a japanese byo-yomi clock for 2 players
(very common on computer Go games), with a handful of hardcoded
presets to choose from at runtime.  This includes as special cases:

* SuddenDeath clock (0 byoyomi periods)
* blitz game with a fixed time for each move (0s main time, 1 byoyomi period)

### User interface ###

- in CONFIG mode (default at startup):
    - D0 = switches to PLAY mode
    - D1 = cycle through settings
    - D2 = cycle through predefined setting values
- in PLAY mode:
    - D0 = PAUSE, again switches to CONFIG mode
    - D1/D2 = players

### Current limitations ###

* UI needs improvement
    * font sizes need to be finetuned
    * should switch to "seconds" display at 90s ?
* no confirmation is asked before loosing current game timings
* no support for chess n-moves-per-period timing
* after pausing, any player may signal restart of opponent's clock
* the atmega32u4 USB support is not used yet, an external USB/serial
  converter has to be connected to the USART pins for a debug console

### Known bugs ###

* the timer sometimes stops spontaneously
* the last preset value for duration gets corrupted after playing (and
  the config UI gets unresponsive after selecting it)
* the debouncing capacitors are apparently not completely sufficient
* maintime=0 has a problem, we offer 1s instead as a workaround

### Future enhancements ###

* hardware
    * capacitive contacts
    * run on LiPo battery, with USB charging
* software
    * permanent settings in EEPROM
    * save game clock state to EEPROM for overnight pause
    * multiple clock mechanism (Fisher, etc)
    * allow to enter custom clock settings
    * different time settings for each player
    * save/manage (multiple) custom settings to EEPROM

### Possible general enhancements for future versions ###

Those require expanding hardware capabilities:

* clock calibration by external signal, EEPROM-saved
* recording of game timings (to microSD ?), to complement moves record
* triggering of an external camera to record the game (wire, bluetooth, whatever)
* generic expansion slot ?  SDIO ?
* backup/restore configuration to PC and/or microSD

### Other things to do ###

* check current consumption
* get rid of Wiring runtime once avr-libc proto is OK enough
